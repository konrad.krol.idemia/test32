FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/test32.sh"]

COPY test32.sh /usr/bin/test32.sh
COPY target/test32.jar /usr/share/test32/test32.jar
